package com.java.base;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 多重for循环优化
 * @Author: hu bing
 * 1）外小内大”原则
 * 2）提取与循环无关的表达式
 * 3）消除循环终止判断时的方法调用
 * 4）异常捕获
 * 5）尝试使用map结构查询的执行方式；
 * 6）尝试使用Java8新特性的Stream流的方式；
 * 7）消除循环终止判断时的方法调用，比如：
 * 8）减少循环变量的实例化，比如：
 * @Date: 2022/10/15
 **/
public class NestedForLoopOptimize {

	/**
	 * 1）外小内大”原则
	 * 2）提取与循环无关的表达式
	 * 3）消除循环终止判断时的方法调用
	 * 4）异常捕获
	 * 5）尝试使用map结构查询的执行方式；
	 * 6）尝试使用Java8新特性的Stream流的方式；
	 * 7）消除循环终止判断时的方法调用，比如：
	 * 8）减少循环变量的实例化，比如：
	 */
	public void forLoop(){
		/**
		 * 外小内大”原则
		 */
		long stratTime = System.nanoTime();
		for (int i = 0; i < 10000000; i++) {
			for (int j = 0; j < 10; j++) {}
		}
		long endTime = System.nanoTime();
		System.out.println("外大内小耗时："+ (endTime - stratTime));
		/** 优化后 */
		long stratTime1 = System.nanoTime();
		for (int i = 0; i <10 ; i++) {
			for (int j = 0; j < 10000000; j++) {
			}
		}
		long endTime1 = System.nanoTime();
		System.out.println("外小内大耗时："+(endTime1 - stratTime1));
		/********************************************************************************************/
		/** 提取与循环无关的表达式 */
		long stratTime2 = System.nanoTime();
		int a=1,b=2;
		for (int i = 0; i < 10000000; i++) {
			i=i*a*b;
		}
		long endTime2 = System.nanoTime();
		System.out.println("未提取耗时："+(endTime2 - stratTime2));
		/**优化后*/
		long stratTime3 = System.nanoTime();
		int c = a*b;
		for (int i = 0; i < 10000000; i++) {
			i=i*c;
		}
		long endTime3 = System.nanoTime();
		System.out.println("未提取耗时："+(endTime3 - stratTime3));
		/********************************************************************************************/
		/**消除循环终止判断时的方法调用*/
		long stratTime4 = System.nanoTime();
		List list = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
		}
		long endTime4 = System.nanoTime();
		System.out.println("未优化list耗时："+(endTime4- stratTime4));
		/**优化后*/
		long stratTime5 = System.nanoTime();
		int size = list.size();
		for (int i = 0; i < size; i++) {
		}
		long endTime5 = System.nanoTime();
		System.out.println("优化list耗时："+(endTime5 - stratTime5));
		/********************************************************************************************/
		/** 异常捕获 */
		long stratTime6 = System.nanoTime();
		for (int i = 0; i < 10000000; i++) {
			try {
			} catch (Exception e) {
			}
		}
		long endTime6 = System.nanoTime();
		System.out.println("在内部捕获异常耗时："+(endTime6 - stratTime6));
		/** 优化后 */
		long stratTime7 = System.nanoTime();
		try {
			for (int i = 0; i < 10000000; i++) {
			}
		} catch (Exception e) {
		}
		long endTime7 = System.nanoTime();
		System.out.println("在外部捕获异常耗时："+(endTime7 - stratTime7));
		/********************************************************************************************/
		/**尝试使用map结构查询的执行方式*/
		List<Member> list1 = new ArrayList<>();
		List<Member> list2 = new ArrayList<>();
		for(Member m2:list2){
			if(m2.getName()==null){
				for(Member m1:list1){
					if(m1.getId().intValue()==m2.getId().intValue()){
						System.out.println(m2.getId()+" Name 值为空!!!");
					}
				}
			}
		}
		/**优化后*/
		//map查询测试
		long s3 = System.currentTimeMillis();
		int mapNumber = 0;
		Map<Integer, Member> map = new HashMap<>();
		for(Member m1:list1){
			map.put(m1.getId(), m1);
		}
		for(Member m2:list2){
			if(m2.getName()==null){
				Member m = map.get(m2.getId());
				if(m!=null){
					System.out.println(m2.getId()+" Name 值为空!!!");
					mapNumber++;
				}
			}
		}
		/********************************************************************************************/
		/**尝试使用Java8新特性的Stream流的方式*/
		/**消除循环终止判断时的方法调用，比如*/
		for ( int j = 0; j<list.size() ; j++){
		}
		/**优化为*/
		for ( int j = 0; j< 10000 ; j++){
		}
		/********************************************************************************************/
		/**减少循环变量的实例化*/
		for (int i = 0; i < 1000; i++){}
		/**优化后*/
		int i, j, k;
		for (i = 0; i < 10; i++){}
	}
}
@Data
class Member{
	private String name;
	private Integer id;
}
