package com.spring;

import org.springframework.lang.Nullable;

/**
 * @Description:
 * @Author: hu bing
 * @Date: 2022/10/22
 **/
public interface FactoryBean<T> {
	/**
	 * 返回创建好的对象
	 */
	@Nullable
	T getObject() throws Exception;
	/**
	 * 返回需要创建的对象的类型
	 */
	@Nullable
	Class<?> getObjectType();
	/**
	 * bean是否是单例的
	 **/
	default boolean isSingleton() {
		return true;
	}
}
