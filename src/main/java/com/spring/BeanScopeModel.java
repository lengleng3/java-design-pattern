package com.spring;

/**
 * @Description:
 * @Author: hu bing
 * @Date: 2022/10/24
 **/
public class BeanScopeModel {
	public BeanScopeModel(String beanScope) {
		System.out.println(String.format("线程:%s,create BeanScopeModel,{sope=%s},{this=%s}", Thread.currentThread(), beanScope, this));
	}
}
