package com.spring;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;
import org.springframework.lang.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @Description:
 * @Author: hu bing
 * @Date: 2022/10/24
 **/
public class ThreadScope implements Scope {
	public static final String THREAD_SCOPE = "thread";
	private ThreadLocal<Map<String, Object>> beanMap = new ThreadLocal() {
		@Override
		protected Object initialValue() {
			return new HashMap<>();
		}
	};

	@Override
	public Object get(String name, ObjectFactory<?> objectFactory) {
		Object bean = beanMap.get().get(name);
		if (Objects.isNull(bean)) {
			bean = objectFactory.getObject();
			beanMap.get().put(name, bean);
		}
		return bean;
	}

	@Override
	public Object remove(String s) {
		return this.beanMap.get().remove(s);
	}

	@Override
	public void registerDestructionCallback(String s, Runnable runnable) {
		/** bean作用域范围结束的时候调用的方法，用于bean清理 */
		System.out.println(s);
	}

	@Nullable
	@Override
	public Object resolveContextualObject(String key) {
		return null;
	}

	@Nullable
	@Override
	public String getConversationId() {
		return Thread.currentThread().getName();
	}
}
