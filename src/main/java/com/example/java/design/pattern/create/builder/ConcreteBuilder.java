package com.example.java.design.pattern.create.builder;

/**
 * @Description: 具体建造者
 * @Author: hu bing
 * @Date: 2022/10/5
 **/
public class ConcreteBuilder extends Builder{
	@Override
	public void buildPartA() {
		product.setPartA("建造 PartA");
	}

	@Override
	public void buildPartB() {
		product.setPartA("建造 PartB");
	}

	@Override
	public void buildPartC() {
		product.setPartA("建造 PartC");
	}
}
