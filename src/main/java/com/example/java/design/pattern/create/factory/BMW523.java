package com.example.java.design.pattern.create.factory;

/**
 * @Description: 523
 * @Author: hu bing
 * @Date: 2022/10/4
 **/
public class BMW523 extends BMW {
	public BMW523(){
		System.out.println("制造-->BMW523");
	}
}
