package com.example.java.design.pattern.create.builder.example;

/**
 * @Description: 具体装修工人2
 * @Author: hu bing
 * @Date: 2022/10/5
 **/
public class ConcreteDecorator2 extends Decorator{
	@Override
	public void buildWall() {
		parlour.setWall("w2");
	}

	@Override
	public void buildTV() {
		parlour.setTV("TV2");
	}

	@Override
	public void buildSofa() {
		parlour.setSofa("sf2");
	}
}
