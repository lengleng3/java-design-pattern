package com.example.java.design.pattern.create.simplefactory;

import com.example.java.design.pattern.create.factory.BMW;
import com.example.java.design.pattern.create.factory.BMW320;
import com.example.java.design.pattern.create.factory.BMW523;

/**
 * @Description: 工厂
 * @Author: hu bing
 * @Date: 2022/10/4
 **/
public class Factory {
	public BMW createBMW(int type){
		switch (type){
			case 320:
				return new BMW320();
			case 523:
				return new BMW523();
			default:
				break;

		}
		return null;
	}
}
