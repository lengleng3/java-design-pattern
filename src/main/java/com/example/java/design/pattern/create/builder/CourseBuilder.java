package com.example.java.design.pattern.create.builder;

/**
 * @Description: 课程建造者
 * @Author: hu bing
 * @Date: 2022/10/5
 **/
public class CourseBuilder {
	private  Course course = new Course();

	public CourseBuilder addName(String name){
		course.setName(name);
		return this;
	}

	public CourseBuilder addMeans(String means){
		course.setMeans(means);
		return this;
	}

	public CourseBuilder addNote(String note){
		course.setNote(note);
		return this;
	}

	public CourseBuilder addHomework(String homework){
		course.setHomework(homework);
		return this;
	}

	public Course build(){
		return course;
	}
}
