package com.example.java.design.pattern.create.builder.example;

/**
 * @Description: 装修工人
 * @Author: hu bing
 * @Date: 2022/10/5
 **/
public abstract class Decorator {
	/**
	 * 创建产品对象
	 */
	protected Parlour parlour = new Parlour();

	public abstract void buildWall();

	public abstract void buildTV();

	public abstract void buildSofa();

	/**
	 * 返回产品对象
	 */
	public Parlour getResult(){
		return parlour;
	}
}
