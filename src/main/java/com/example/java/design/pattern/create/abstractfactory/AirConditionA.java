package com.example.java.design.pattern.create.abstractfactory;

/**
 * @Description: AirConditionA
 * @Author: hu bing
 * @Date: 2022/10/4
 **/
public class AirConditionA implements AirCondition{
	public AirConditionA(){
		System.out.println("制造-->AirConditionA");
	}
}
