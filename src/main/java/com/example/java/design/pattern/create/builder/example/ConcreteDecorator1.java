package com.example.java.design.pattern.create.builder.example;

/**
 * @Description: 具体装修工人1
 * @Author: hu bing
 * @Date: 2022/10/5
 **/
public class ConcreteDecorator1 extends Decorator{
	@Override
	public void buildWall() {
		parlour.setWall("w1");
	}

	@Override
	public void buildTV() {
		parlour.setTV("TV1");
	}

	@Override
	public void buildSofa() {
		parlour.setSofa("sf1");
	}
}
