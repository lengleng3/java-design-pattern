package com.example.java.design.pattern.create.builder.example;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * @Description: 读XML
 * @Author: hu bing
 * @Date: 2022/10/5
 **/
public class ReadXML {
	public static Object getObject(){
		try{
			DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dFactory.newDocumentBuilder();
			Document doc = builder.parse(new File("src/Builder/config.xml"));
			NodeList nl = doc.getElementsByTagName("className");
			Node classNode = nl.item(0).getFirstChild();
			String cName = "Builder."+classNode.getNodeValue();
			System.out.println("新类名:"+cName);
			Class<?> c = Class.forName(cName);
			Object obj = c.newInstance();
			return obj;
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
}
