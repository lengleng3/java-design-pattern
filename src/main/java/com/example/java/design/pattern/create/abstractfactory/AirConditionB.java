package com.example.java.design.pattern.create.abstractfactory;

/**
 * @Description: AirConditionB
 * @Author: hu bing
 * @Date: 2022/10/4
 **/
public class AirConditionB implements AirCondition{
	public AirConditionB(){
		System.out.println("制造-->AirConditionB");
	}
}
