package com.example.java.design.pattern.create.builder;

import lombok.Setter;

/**
 * @Description: 产品
 * @Author: hu bing
 * @Date: 2022/10/5
 **/
@Setter
public class Product {
	private String partA;
	private String partB;
	private String partC;

	public void show(){
		/**显示产品特性*/
		System.out.println(partA);
		System.out.println(partB);
		System.out.println(partC);
	}
}
