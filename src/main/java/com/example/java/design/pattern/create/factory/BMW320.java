package com.example.java.design.pattern.create.factory;

/**
 * @description 320
 * @author hu bing
 * @Date: 2022/10/4
 **/
public class BMW320 extends BMW {
	public BMW320(){
		System.out.println("制造-->BMW320");
	}
}
