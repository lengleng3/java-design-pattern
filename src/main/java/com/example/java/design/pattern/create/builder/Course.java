package com.example.java.design.pattern.create.builder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @Description: 课程类
 * @Author: hu bing
 * @Date: 2022/10/5
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Course {
	/**
	 * 课程名称
	 */
	private String name;
	/**
	 * 教程资料
	 */
	private String means;
	/**
	 * 学习笔记
	 */
	private String note;
	/**
	 * 课后作业
	 */
	private String homework;

	@Override
	public String toString() {
		return "CourseBuilder{" + "name='" + name + '\'' + ",means='" + means + '\'' + ",note='" + note + '\'' + ",homework='" + homework + '\'' + '}';
	}
}

