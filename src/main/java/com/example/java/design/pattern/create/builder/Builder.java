package com.example.java.design.pattern.create.builder;

/**
 * @Description: 抽象建造者
 * @Author: hu bing
 * @Date: 2022/10/5
 **/
public abstract class Builder {
	/**创建产品对象*/
	protected Product product = new Product();

	public abstract void buildPartA();

	public abstract void buildPartB();

	public abstract void buildPartC();

	/**返回产品对象*/
	public Product getResult(){
		return product;
	}
}
