package com.example.java.design.pattern.create.builder;

/**
 * @Description: 指挥者
 * @Author: hu bing
 * @Date: 2022/10/5
 **/
public class Director {
	private Builder builder;
	public Director(Builder builder){
		this.builder=builder;
	}
	public Product construct(){
		builder.buildPartA();
		builder.buildPartB();
		builder.buildPartC();
		return builder.getResult();
	}
}
