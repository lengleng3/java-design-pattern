package com.example.java.design.pattern.create.builder.example;

import lombok.AllArgsConstructor;

/**
 * @Description: 指挥者-项目经理
 * @Author: hu bing
 * @Date: 2022/10/5
 **/
@AllArgsConstructor
public class ProjectManager {
	private Decorator builder;
	/**产品构建与组装方法*/
	public Parlour decorate(){
		builder.buildWall();
		builder.buildTV();
		builder.buildSofa();
		return builder.getResult();
	}
}
