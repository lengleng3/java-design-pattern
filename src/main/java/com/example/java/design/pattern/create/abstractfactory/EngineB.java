package com.example.java.design.pattern.create.abstractfactory;

/**
 * @Description: 发动机B
 * @Author: hu bing
 * @Date: 2022/10/4
 **/
public class EngineB implements Engine{
	public EngineB(){
		System.out.println("制造-->EngineB");
	}
}
