package com.example.java.design.pattern.create.builder.example;

import lombok.Setter;

import javax.swing.*;
import java.awt.*;

/**
 * @Description: 客厅
 * @Author: hu bing
 * @Date: 2022/10/5
 **/
@Setter
public class Parlour {
	/**
	 * 墙
	 */
	private String wall;
	/**
	 * 电视
	 */
	private String TV;
	/**
	 * 沙发
	 */
	private String sofa;

	public void show(){
		JFrame jf = new JFrame("建造者模式测试");
		Container container = jf.getContentPane();
		JPanel p = new JPanel();
		JScrollPane sp = new JScrollPane();
		String parlour = wall+TV+sofa;
		JLabel l = new JLabel(new ImageIcon("src/"+parlour+".jpg"));
		p.setLayout(new GridLayout(1,1));
		p.add(l);
		container.add(sp,BorderLayout.CENTER);
		jf.pack();
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
