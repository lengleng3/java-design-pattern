package com.example.java.design.pattern.create.factory;

import com.example.java.design.pattern.create.abstractfactory.*;

/**
 * @Description: 具体工厂
 * @Author: hu bing
 * @Date: 2022/10/4
 **/
public class FactoryBMW320 implements FactoryBMW, AbstractFactory {
	@Override
	public Engine createEngine() {
		return new EngineA();
	}

	@Override
	public AirCondition createAriCondition() {
		return new AirConditionA();
	}

	@Override
	public BMW createBMW() {
		return new BMW320();
	}
}
