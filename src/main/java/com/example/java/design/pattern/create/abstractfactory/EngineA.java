package com.example.java.design.pattern.create.abstractfactory;

/**
 * @Description: 发动机A
 * @Author: hu bing
 * @Date: 2022/10/4
 **/
public class EngineA implements Engine{
	public EngineA(){
		System.out.println("制造-->EngineA");
	}
}
