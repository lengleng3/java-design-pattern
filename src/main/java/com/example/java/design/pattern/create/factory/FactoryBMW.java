package com.example.java.design.pattern.create.factory;

/**
 * @author hu bing
 * @className FactoryBMW
 * @Description 宝马工厂
 * date 22-10-4下午 14：20
 * @version 1.0
 */
public interface FactoryBMW {
	BMW createBMW();
}
