package com.example.java.design.pattern.create.abstractfactory;

import com.example.java.design.pattern.create.factory.BMW;

/**
 * @Description: 抽象工厂
 * @Author: hu bing
 * @Date: 2022/10/4
 **/
public interface AbstractFactory {
	/**
	 * 制造发动机
	 */
	Engine createEngine();

	/**
	 * 制造空调
	 */
	AirCondition createAriCondition();

	/**
	 * 制造汽车
	 */
	BMW createBMW();
}
