package com.util;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Description:
 * @Author: hu bing
 * @Date: 2022/10/25
 **/
public class IocUtils {
	public static ClassPathXmlApplicationContext context(String beanXml) {
		return new ClassPathXmlApplicationContext(beanXml);
	}
}
