package spring;

import com.spring.ThreadScope;
import com.util.IocUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * @Description:
 * @Author: hu bing
 * @Date: 2022/10/22
 **/
public class SpringTest {
	@Test
	public void beanXmlTest() {
		/** 1.bean配置文件位置 */
		String beanXml = "classpath*:spring/beans.xml";
		/** 2.创建ClassPathXmlApplicationContext容器，给容器指定需要加载的bean配置文件 */
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
		for (String beanName : Arrays.asList("user1", "user2", "user3", "user4", "user5")) {
			/** 获取bean的别名 */
			String[] aliases = context.getAliases(beanName);
			System.out.println(String.format("beanName:%s,别名:[%s]", beanName, String.join(",", aliases)));
		}
		System.out.println("spring容器中所有bean如下：");
		/** getBeanDefinitionNames用于获取容器中所有bean的名称 */
		for (String beanName : context.getBeanDefinitionNames()) {
			/**获取bean的别名*/
			String[] aliases = context.getAliases(beanName);
			System.out.println(String.format("beanName:%s,别名:[%s]", beanName, String.join(",", aliases)));
		}
		/** getBeanDefinitionNames用于获取容器中所有bean的名称 */
		for (String beanName : context.getBeanDefinitionNames()) {
			System.out.println(beanName + ":" + context.getBean(beanName));
		}
	}

	@Test
	public void beanXmlStaticFactoryTest() {
		/** 1.bean配置文件位置 */
		String beanXml = "classpath*:spring/beans.xml";
		/** 2.创建ClassPathXmlApplicationContext容器，给容器指定需要加载的bean配置文件 */
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
		System.out.println("spring容器中所有bean如下：");
		for (String beanName : context.getBeanDefinitionNames()) {
			System.out.println(beanName + ":" + context.getBean(beanName));
		}
	}

	@Test
	public void threadScopeTest() throws InterruptedException {
		String beanXml = "classpath*:spring/beans-thread.xml";
		/** 手动创建容器 */
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext() {
			@Override
			protected void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
				/** 向容器中注册自定义的scope */
				beanFactory.registerScope(ThreadScope.THREAD_SCOPE, new ThreadScope());
				super.postProcessBeanFactory(beanFactory);
			}
		};
		/** 设置配置文件位置 */
		context.setConfigLocation(beanXml);
		/** 启动容器 */
		context.refresh();
		/** 使用容器获取bean */
		for (int i = 0; i < 2; i++) {
			new Thread(() -> {
				System.out.println(Thread.currentThread() + "," + context.getBean("threadBean"));
				System.out.println(Thread.currentThread() + "," + context.getBean("threadBean"));
			}).start();
			TimeUnit.SECONDS.sleep(1);
		}
	}

	/**
	 * 通过构造器的参数位置注入
	 */
	@Test
	public void diByConstructorParamIndex() {
		String beanXml = "classpath*:spring/diByConstructorParamIndex.xml";
		ClassPathXmlApplicationContext context = IocUtils.context(beanXml);
		System.out.println(context.getBean("diByConstructorParamIndex"));
	}

	/**
	 * 通过构造器的参数类型注入
	 */
	@Test
	public void diByConstructorParamType() {
		String beanXml = "classpath*:spring/diByConstructorParamType.xml";
		ClassPathXmlApplicationContext context = IocUtils.context(beanXml);
		System.out.println(context.getBean("diByConstructorParamType"));
	}
}
