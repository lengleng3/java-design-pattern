package com.example.java.design.pattern.create;

import com.example.java.design.pattern.create.factory.*;
import com.example.java.design.pattern.create.simplefactory.Factory;
import org.junit.jupiter.api.Test;

/**
 * @Description: 工厂模式测试类
 * @Author: hu bing
 * @Date: 2022/10/4
 **/
public class FactoryTest {
	@Test
	public void simpleFactoryTest(){
		Factory factory = new Factory();
		BMW bmw320 = factory.createBMW(320);
		BMW bmw523 = factory.createBMW(523);
	}

	/**
	 * 工厂模式
	 */
	@Test
	public void factoryTest(){
		FactoryBMW320 factoryBMW320 = new FactoryBMW320();
		BMW320 bmw320 = (BMW320) factoryBMW320.createBMW();

		FactoryBMW523 factoryBMW523 = new FactoryBMW523();
		BMW523 bmw523 = (BMW523) factoryBMW523.createBMW();
	}
	/**
	 * 抽象工厂
	 */
	@Test
	public void abstractFactoryTest(){
		/**生产宝马320系列配件*/
		FactoryBMW320 factoryBMW320 = new FactoryBMW320();
		factoryBMW320.createEngine();
		factoryBMW320.createAriCondition();

		/**生产宝马523系列配件*/
		FactoryBMW523 factoryBMW523 = new FactoryBMW523();
		factoryBMW523.createEngine();
		factoryBMW523.createAriCondition();
	}
}
