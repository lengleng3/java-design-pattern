package com.example.java.design.pattern.create;

import com.example.java.design.pattern.create.builder.*;
import com.example.java.design.pattern.create.builder.example.Decorator;
import com.example.java.design.pattern.create.builder.example.Parlour;
import com.example.java.design.pattern.create.builder.example.ProjectManager;
import com.example.java.design.pattern.create.builder.example.ReadXML;
import org.junit.jupiter.api.Test;

/**
 * @Description: 建造者模式测试类
 * @Author: hu bing
 * @Date: 2022/10/5
 **/
public class BuilderTest {

	@Test
	public void builderTest(){
		Builder builder = new ConcreteBuilder();
		Director director= new Director(builder);
		Product product = director.construct();
		product.show();
	}

	@Test
	public void parlourDecoratorTest(){
		try{
			Decorator d = (Decorator) ReadXML.getObject();
			ProjectManager m = new ProjectManager(d);
			Parlour p = m.decorate();
			p.show();
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void courseBuilder(){
		Course course = new CourseBuilder()
			.addName("设计模式")
			.addMeans("教程资料")
			.addNote("课堂笔记")
			.addHomework("课后作业")
			.build();
		System.out.println(course);
	}

	@Test
	public void innerCourseBuilderTest(){
		Course course = Course.builder()
			.name("设计模式")
			.means("教程资料")
			.note("课堂笔记")
			.homework("课后作业")
			.build();
		System.out.println(course);
	}
}
