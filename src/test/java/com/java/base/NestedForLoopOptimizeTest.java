package com.java.base;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

/**
 * @Description: 多重for循环优化测试类
 * @Author: hu bing
 * @Date: 2022/10/15
 **/
public class NestedForLoopOptimizeTest {

	@InjectMocks
	private NestedForLoopOptimize nestedForLoopOptimize;

	public NestedForLoopOptimizeTest(){
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void forLoopTest(){
		nestedForLoopOptimize.forLoop();
	}
}
